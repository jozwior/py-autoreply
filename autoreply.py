#!/usr/bin/env python

import datetime
import imaplib
import smtplib
from email import message_from_bytes
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import make_msgid

BODY = """
<p>Hello</p>
"""

# Settings
date = datetime.date.today().strftime("%d-%b-%Y")
imap_server = "imap.gmail.com"
smtp_server = "smtp.gmail.com"
user_login = ""
user_password = ""
mailbox_select = "INBOX"

# SMTP
smtp = smtplib.SMTP_SSL(smtp_server)
smtp.login(user_login, user_password)

to_format_search_criteria = 'SUBJECT "Some Subject" UNANSWERED SINCE "{}"'
search_criteria = to_format_search_criteria.format(date)

# IMAP
mail = imaplib.IMAP4_SSL(imap_server)
mail.login(user_login, user_password)
mail.select(mailbox_select)


def answer(original_subject, reply_to, body, original, user_login):
    reply = MIMEMultipart('alternative')
    reply['Message-ID'] = make_msgid()
    reply['References'] = reply['In-Reply-To'] = original['Message-ID']
    reply['Subject'] = 'Re: ' + original_subject
    reply['From'] = user_login
    reply['To'] = reply_to
    reply.attach(MIMEText(BODY, 'html'))
    return reply


_, message_ids_data = mail.search(None, search_criteria)
print(message_ids_data)
for message_id_bytes in message_ids_data[0].split():
    message_id = message_id_bytes.decode("utf-8")
    print(message_id)
    __, message_datas = mail.fetch(message_id, '(RFC822)')
    for message_data in message_datas:
        if isinstance(message_data, tuple):
            plain_message = message_from_bytes(message_data[1])
            reply_to = plain_message['Reply-To']
            subject = plain_message['Subject']
            if subject.startswith('Some Subject'):
                reply_message = answer(subject, reply_to, BODY, plain_message, user_login)
                smtp.sendmail(user_login, reply_to, reply_message.as_bytes())
                mail.store(message_id, '+FLAGS', '\\Answered')
    mail.store(message_id, '-FLAGS', '\\Seen')  # Mark email as Unread
